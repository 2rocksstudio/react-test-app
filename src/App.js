import React from 'react'
import {BrowserRouter, Routes, Route, Navigate, Link} from 'react-router-dom'
import './App.css'

import Manatee from './components/Manatee/Manatee'
import Narwhal from './components/Narwhal/Narwhal'
import Whale from './components/Whale/Whale'
import Home from './components/Home'

function App() {
  return (
    <div className="wrapper">
      <h1>Marine Mammals edit</h1>
      <BrowserRouter>
        <nav>
          <ul>
            <li><Link to="/manatee">Manatee</Link></li>
            <li><Link to="/narwhal">Narwhal</Link></li>
            <li><Link to="/whale">Whale</Link></li>
            <li><Link to="/whale/beluga">Beluga Whale</Link></li>
            <li><Link to="/whale/blue">Blue Whale</Link></li>
          </ul>
        </nav>
        <Routes>
          <Route path="*" element={<Navigate to="/home" />} />
          <Route
            path="/home"
            element={
              <Home />
            }
          />
          <Route
            path="/manatee"
            element={
              <Manatee />
            }
          />
          <Route
            path="/narwhal"
            element={
              <Narwhal />
            }
          />
          <Route
            path="/whale/*"
            element={
              <Whale />
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
